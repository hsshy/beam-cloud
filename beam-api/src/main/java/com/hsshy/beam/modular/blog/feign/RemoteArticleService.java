package com.hsshy.beam.modular.blog.feign;
import com.hsshy.beam.common.constant.ServiceNameConstants;
import com.hsshy.beam.common.utils.R;
import com.hsshy.beam.modular.blog.entity.Article;
import com.hsshy.beam.modular.blog.feign.factory.RemoteArticleServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
@FeignClient(contextId = "remoteArticleService",name = ServiceNameConstants.ADMIN_SERVICE,fallbackFactory = RemoteArticleServiceFallbackFactory.class)
public interface RemoteArticleService {

    @GetMapping(value = "/beam-admin/api/blog/detail/{shortCode}",produces = "application/json;charset=utf-8")
    R<Article> getArticleInfo(@PathVariable("shortCode") String shortCode);



}