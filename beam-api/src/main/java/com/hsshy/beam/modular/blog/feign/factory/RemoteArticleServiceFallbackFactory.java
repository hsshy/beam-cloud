package com.hsshy.beam.modular.blog.feign.factory;
import com.hsshy.beam.modular.blog.feign.RemoteArticleService;
import com.hsshy.beam.modular.blog.feign.fallback.RemoteArticleServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @description: 获取错误信息，并进行
 * @author: hs
 * @create: 2019-12-10 14:35:35
 **/
@Slf4j
@Component
public class RemoteArticleServiceFallbackFactory implements FallbackFactory<RemoteArticleService> {
    @Autowired
    private RemoteArticleServiceFallbackImpl remoteArticleServiceFallback;

    @Override
    public RemoteArticleService create(Throwable throwable) {
        log.error("进入回退逻辑", throwable);
//        RemoteArticleServiceFallbackImpl remoteArticleServiceFallback = new RemoteArticleServiceFallbackImpl();
        remoteArticleServiceFallback.setCause(throwable);
        return remoteArticleServiceFallback;
    }
}
