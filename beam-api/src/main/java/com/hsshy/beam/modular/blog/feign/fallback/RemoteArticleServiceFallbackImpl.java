package com.hsshy.beam.modular.blog.feign.fallback;

import com.hsshy.beam.common.utils.R;
import com.hsshy.beam.modular.blog.entity.Article;
import com.hsshy.beam.modular.blog.feign.RemoteArticleService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @description: 调用失败逻辑处理类
 * @author: hs
 * @create: 2019-12-10 14:29:30
 **/
@Slf4j
@Component
public class RemoteArticleServiceFallbackImpl implements RemoteArticleService {

    @Setter
    private Throwable cause;

    @Override
    public R<Article> getArticleInfo(String shortCode) {
        log.error("获取文章详情失败", cause);
        return R.fail("获取文章详情失败");
    }
}
