package com.hsshy.beam.modular.blog.controller;
import com.hsshy.beam.common.utils.R;
import com.hsshy.beam.modular.blog.entity.Article;
import com.hsshy.beam.modular.blog.feign.RemoteArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class ArticleController {

    @Autowired
    private RemoteArticleService remoteArticleService;

    @GetMapping(value = "/article/{shortCode}",produces = "application/json;charset=utf-8")
    public R<Article> articleInfo(@PathVariable String shortCode) {
        R<Article> r = remoteArticleService.getArticleInfo(shortCode);
        return r;
    }


    //    @Autowired
//    RestTemplate restTemplate;
//
//    @GetMapping(value = "/article/{shortCode}",produces = "application/json;charset=utf-8")
//    public R<Article> articleInfo(@PathVariable String shortCode) {
//        R r = this.restTemplate.getForObject(
//                "http://beam-admin/beam-admin/api/blog/detail/{shortCode}",
//                R.class,
//                shortCode
//        );
//        return r;
//    }

}