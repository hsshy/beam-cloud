package com.hsshy.beam;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
@EnableZuulProxy
@SpringBootApplication
public class BeamGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeamGatewayApplication.class, args);
	}

}
