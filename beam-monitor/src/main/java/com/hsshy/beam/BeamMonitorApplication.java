package com.hsshy.beam;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class BeamMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeamMonitorApplication.class, args);
	}

}
